#include <pybind11/pybind11.h>

#include "../app/Izing.h"


#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>
#include <pybind11/numpy.h>



namespace py = pybind11;

using VectorInt = std::vector<int, std::allocator<int>>;
using VectorDouble = std::vector<double, std::allocator<double>>;

using VectorOfVectorInt = std::vector<std::vector<int, std::allocator<int>>>;

PYBIND11_MAKE_OPAQUE(std::vector<int>);
PYBIND11_MAKE_OPAQUE(std::vector<double>);
PYBIND11_MAKE_OPAQUE(VectorOfVectorInt);
PYBIND11_MODULE(pybind11module, m)
{
	py::bind_vector<std::vector<int>>(m, "VectorInt", py::buffer_protocol());
	py::bind_vector<std::vector<double>>(m, "VectorDouble", py::buffer_protocol());

    py::class_<VectorOfVectorInt>(m, "VectorOfVectorInt")
        .def(py::init<>())
        .def("clear", &VectorOfVectorInt::clear)
        .def("push_back", (void (VectorOfVectorInt::*)(const VectorInt&)) & VectorOfVectorInt::push_back)
        .def("__len__", [](const VectorOfVectorInt& v) { return v.size(); })
        .def("__iter__", [](VectorOfVectorInt& v) {
        return py::make_iterator(v.begin(), v.end());
            }, py::keep_alive<0, 1>())
        ;




	m.doc() = "Pybind11Module";

	m.def("start_simulate", &start_simulate);
}
