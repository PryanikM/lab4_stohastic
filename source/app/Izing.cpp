#include "izing.h"
#include <format>
void compute_energy_and_magnetiz
(
	double& E,
	double& M,
	const int N,
	const double J,
	const double H,
	const std::vector<int>& grid
)
{
	M = std::accumulate(grid.begin(), grid.end(), 0);
	E = 0;
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			E += -H * grid[i * N + j];
			if (i + 1 != N) {
				E += -J * grid[i * N + j] * grid[(i + 1) * N +j] ;
			}
			else {
				E += -J * grid[i * N + j] * grid[j];
			}
			if (j + 1 != N) {
				E += -J * grid[i * N + j] * grid[i * N + j + 1];
			}
			else {
				E += -J * grid[i * N + j] * grid[i * N];
			}

		}
	}
}

int sum_neighbors
(
	const std::vector<int>& grid,
	const int N,
	const int x,
	const int y
)
{
	int sum = 0;

	if (y == 0) {
		sum += grid[(N - 1) * N + x];
	}
	else {
		sum += grid[(y - 1) * N + x];
	}

	if (y + 1 == N) {
		sum += grid[x];
	}
	else {
		sum += grid[(y + 1) * N + x];
	}

	if (x == 0) {
		sum += grid[y * N + N - 1];
	}
	else {
		sum += grid[y * N + x - 1];
	}

	if (x + 1 == N) {
		sum += grid[y * N];
	}
	else {
		sum += grid[y * N + x + 1];
	}

	return sum;
}


void delta_energy
(
	double& delta_e,
	const double J,
	const double H,
	const std::vector<int>& grid,
	int N,
	int x,
	int y
)
{
	delta_e = 2. * J * grid[x + y * N] * sum_neighbors(grid, N, x, y) + 2. * H * grid[x + y * N];
}

void ising_update
(
	double& E,
	double& M,
	int N,
	const std::vector<int>& grid,
	const double J,
	const double H,
	const double t
)
{
	double delta_e = 0.;
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			
		}
	}
}

void start_simulate(
	std::vector<int>& grid,
	std::vector<double>& X,
	std::vector<double>& Cv,
	std::vector<double>& M_sr,
	std::vector<double>& E_sr,
	std::vector<double>& temperatures,
	std::vector<std::vector<int>>& results,
	const int N,
	const int mc_steps,
	const double J,
	const double H
)
{
	std::cout << "start \n";

	int nT = temperatures.size();
	double M = 0;
	double E = 0;
	double E_sum = 0;
	double M_sum = 0;
	double E_sum_2 = 0;
	double M_sum_2 = 0;
	double delta_e = 0.;
	double temperature = 0.;

	compute_energy_and_magnetiz(E, M, N, J, H, grid);
	std::cout << "E=" << E << "M=" << M << "H="<<H<<"\n";
	for (int t = 0; t < nT; ++t) {
		std::cout << "t = " << t << "\n";
		temperature = temperatures[t];
		//std::cout << "M = " << M << "\n";
		E_sum = 0.;
		M_sum = 0.;
		E_sum_2 = 0.;
		M_sum_2 = 0.;
		for (int j = 0; j < mc_steps; ++j) {

			for (int y = 0; y < N; ++y) {
				for (int x = 0; x < N; ++x) {
					
					delta_energy(delta_e, J, H, grid, N, x, y);
					//std::cout << exp((-1. * delta_e) / temperature) << " >= " << static_cast <double> (rand()) / static_cast <float> (RAND_MAX) << "\n";
					if ((delta_e < 0) || std::min(exp((-1. * delta_e) / temperature), 1.) >= static_cast <double> (rand()) / static_cast <float> (RAND_MAX)) {
						grid[x + y * N] *= -1;
						M += 2 * grid[x + y * N];
						E += delta_e;
						//double EE, MM;
						//compute_energy_and_magnetiz(EE, MM, N, J, H, grid);
						//std::cout << delta_e<<":  E=" << E << " EE=" << EE << "\n";
						//if (std::abs(E - EE) > 0.0001) {
						//	throw std::exception("Error");
						//}
					}


				}
			}
			E_sum = E_sum + E;

			M_sum = M_sum + M;
			E_sum_2 = E_sum_2 + E * E;
			M_sum_2 = M_sum_2 + M * M;


		}
		M_sr[t] = M_sum / (N * N  * mc_steps);
		E_sr[t] = E_sum / (N * N * mc_steps);
		
		
		X[t] = (1. / temperature) * (M_sum_2 / (N * N * N * N * mc_steps) - M_sr[t] * M_sr[t]);
		//std::cout << "X[t] = " << X[t] << "; M_sr = " << M_sr[t] << "; M_sum / N^4 = " << M_sum_2 / (N * N * N * N * mc_steps) << "\n";
		Cv[t] = (1. / temperature * temperature) * (E_sum_2 / (N * N * N * N * mc_steps) - E_sr[t] * E_sr[t]);
		results.push_back(grid);
	}

}
