#pragma once
#include <vector>
#include <iostream>
#include <numeric>
#include <cmath>
#include <algorithm>
 
void start_simulate
(
	std::vector<int>& grid,
	std::vector<double>& X,
	std::vector<double>& Cv,
	std::vector<double>& M_sr,
	std::vector<double>& E_sr,
	std::vector<double>& temperatures,
	std::vector<std::vector<int>>& results,
	const int N,
	const int mc_steps,
	const double J,
	const double H
);

void compute_energy_and_magnetiz
(
	double& E,
	double& M,
	const int N,
	const double J,
	const double H,
	const std::vector<int>& grid
);

void ising_update
(
	double& E,
	double& M,
	int N, 
	const std::vector<int>& grid,
	const double J,
	const double H,
	const double t
);

void delta_energy
(
	double& delta_e,
	const double J,
	const double H,
	const std::vector<int>& grid,
	const int N,
	const int x,
	const int y
);

int sum_neighbors
(
	const std::vector<int>& grid,
	const int N,
	const int x,
	const int y
);