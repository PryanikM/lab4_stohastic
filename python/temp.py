import copy

import numpy as np
import random
import matplotlib.pyplot as plt
from pybind11module import start_simulate, VectorInt, VectorDouble, VectorOfVectorInt
import os
def generate_spin(N, trans_percent):
    init_random_arr = np.random.random((N, N))
    grid = np.ones((N, N), dtype=np.integer)
    grid[init_random_arr >= trans_percent] = 1
    grid[init_random_arr < trans_percent] = -1
    return grid

N = 100  # Размер сетки
mc_steps = 1000# Число повторов Монте-Карло
trans_percent = 0.6 # Вероятность, при инициализации, выпадения -1
J = 0.5
H = 0.
T_start = 0.1
T_end = 1.2
nT = 30
is_save = False
grid = generate_spin(N, trans_percent)
# fig0, ax0 = plt.subplots(1)
# pos = ax0.imshow(grid)
# fig0.colorbar(pos, ax=ax0)
# plt.show()

cppGrid = VectorInt()
for g in grid.ravel():
    cppGrid.append(g)

cpp_X = VectorDouble()
cpp_Cv = VectorDouble()
cpp_M_sr = VectorDouble()
cpp_E_sr = VectorDouble()
cpp_results = VectorOfVectorInt()

for i in range(nT):
    cpp_X.append(0.0)
    cpp_Cv.append(0.0)
    cpp_M_sr.append(0.0)
    cpp_E_sr.append(0.0)

T_arr = np.linspace(T_start, T_end, nT)

temperatures = VectorDouble()

for t in T_arr:
    temperatures.append(t)

start_simulate(cppGrid, cpp_X, cpp_Cv, cpp_M_sr, cpp_E_sr, temperatures, cpp_results, N, mc_steps, J, H)

X = np.array(cpp_X)
Cv = np.array(cpp_Cv)
M_sr = np.array(cpp_M_sr)
E_sr = np.array(cpp_E_sr)
Grid = np.array(cppGrid).reshape([N, N])
# results = np.array(cpp_results).reshape([nT, N])
path_to_save = os.path.split(os.getcwd())[0] + "/image_result" + "/"
if not os.path.exists(path_to_save):
    os.makedirs(path_to_save)
if is_save:
    for pos, i in enumerate(cpp_results):
        fig_save, ax_save = plt.subplots(1)
        img = ax_save.imshow(np.array(i).reshape([N, N]), vmin=-1, vmax=1)
        fig_save.colorbar(img, ax=ax_save, ticks=[-1, 1])
        plt.savefig(f'{path_to_save}_{pos}.png')
        plt.clf()
        plt.close(fig_save)


fig, (ax1, ax2) = plt.subplots(2)
fig2, (ax3, ax4) = plt.subplots(2)
line1, = ax1.plot(T_arr, Cv, label="Cv", linewidth=2)
line2, = ax2.plot(T_arr, E_sr, label="E_sr", linewidth=2)
line3, = ax3.plot(T_arr, abs(M_sr), label="M_sr", linewidth=2)
line4, = ax4.plot(T_arr, X, label="X", linewidth=2)

ax1.legend(handles=[line1])
ax2.legend(handles=[line2])
ax3.legend(handles=[line3])
ax4.legend(handles=[line4])
plt.show()





